<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Activity: Access Modifiers and Encapsulation</title>
    </head>
    <body>
        <h1>Building</h1>
        <p>The name of the building is <?php echo $building->getName(); ?>.</p>

        <p><?php echo "The " . $building->getName() . " has " . $building->getFloors() . " floors."; ?></p>

        <p><?php echo "The " . $building->getName() . " is located at " . $building->getAddress() . "."; ?></p>

        <p>The name of the building has been changed to <?php echo $building->setName('Caswynn Complex')->getName(); ?></p>

        <h1>Condominium</h1>
        <p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>

        <p><?php echo "The " . $condominium->getName() . " has " . $condominium->getFloors() . " floors."; ?></p>

        <p><?php echo "The " . $condominium->getName() . " is located at " . $condominium->getAddress() . "."; ?></p>

        <p>The name of the building has been changed to <?php echo $condominium->setNAme('Enzo Tower')->getName(); ?></p>


    </body>
</html>